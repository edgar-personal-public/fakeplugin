function getEngine(year = "") {
  const selectorEngine = "#alternativa-" + year + " option";
  var arrayEngine = [];

  jQuery(selectorEngine).each(function (i, v) {
    if (jQuery(v).val() != "") {
      arrayEngine.push({
        "value": jQuery(v).val(),
        "label": jQuery(v).text(),
      });
    }
  })
  return arrayEngine;
}

function getYears(modelo = "") {
  console.log("modelo" + modelo);
  if (modelo == "Up!") { return [] };
  const selectorYear = "#alternativa-" + modelo + " option";
  var arrayYears = [];

  if (jQuery(selectorYear)) {
    jQuery(selectorYear).each(function (i, v) {
      if (jQuery(v).val() != "") {
        var cYears = jQuery(v).val();
        arrayYears.push({
          "value": jQuery(v).val(),
          "label": jQuery(v).text(),
          "engines": getEngine(jQuery(v).val())
        });
      }
    })
  }
  return arrayYears;
}

function getModels(marca = "") {

  const selectorMarca = "#alternativa-" + marca + " option";
  var modelsObject = {}; // Cambiado de [] a {}
  if (jQuery(selectorMarca)) {
    jQuery(selectorMarca).each(function (i, v) {

      if (jQuery(v).val() != "" && jQuery(v).val() != "UP!") {
        var cModel = jQuery(v).val();
        modelsObject[cModel] = {
          "value": jQuery(v).val(),
          "label": jQuery(v).text(),
          "years": getYears(jQuery(v).val())
        };
      }
    })
  }
  return modelsObject;
}

function getDataForBrand(brand = "") {
  return getModels(brand);
}

function downloadJSON(data, filename) {
  var json = JSON.stringify(data, null, 2);
  var blob = new Blob([json], { type: "application/json" });
  var url = URL.createObjectURL(blob);
  var a = document.createElement("a");
  a.href = url;
  a.download = filename;
  document.body.appendChild(a);
  a.click();
  document.body.removeChild(a);
}

function getAllBrands() {

  var dataBrands = {}; // Cambiado de [] a {}
  jQuery("#marca option").each(function (i,v) {
    var cBrand = jQuery(v).val();
    if (cBrand != "" && cBrand != "UP!" ) {
      dataBrands[cBrand] = getDataForBrand(cBrand);
    }

  });
  return dataBrands;
}

var dataBrandtoJson = getAllBrands();
downloadJSON(dataBrandtoJson, "dataBrand.json");





